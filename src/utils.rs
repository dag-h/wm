use crate::client::Client;
use crate::monitor::Monitor;
use log::info;

/// Gets a new window's centered placement from the monitor size, monitor position, and window
/// size.
pub fn get_centered_placement(mon: &Monitor, client: &Client) -> (i32, i32) {
    if client.width >= mon.width || client.height >= mon.height {
        info!("Window too large, setting pos at origin");
        (mon.x_org as i32, mon.y_org as i32)
    } else {
        let x: i32 = mon.x_org as i32 + mon.width as i32 / 2 - client.width as i32 / 2;
        let y: i32 = mon.y_org as i32 + mon.height as i32 / 2 - client.height as i32 / 2;
        // info!(
        //     "Centered placement: ({},{}) from monitor: {}x{}, ({},{}) and client: {}x{})",
        //     x,
        //     y,
        //     mon.width,
        //     mon.height,
        //     mon.x_org,
        //     mon.y_org,
        //     client.width.get(),
        //     client.height.get()
        // );
        (x, y)
    }
}

/// Gets the relative position within the given monitor from an absolute position
/// on the root window
///
/// TODO: Change to trait bound
pub fn get_relative_pos(mon: &Monitor, x: i32, y: i32) -> (i32, i32) {
    let x_rel = x - mon.x_org as i32;
    let y_rel = y - mon.y_org as i32;
    // info!(
    //     "Got relative pos {},{} from (x,y) = ({},{}), (x_org, y_org) = ({}, {})",
    //     x_rel, y_rel, x, y, mon.x_org, mon.y_org
    // );
    (x_rel, y_rel)
}

/// Gets the absolute position on the root window from a position relative to the
/// given monitors position.
pub fn get_absolute_pos(mon: &Monitor, x: i32, y: i32) -> (i32, i32) {
    let x_abs = x + mon.x_org as i32;
    let y_abs = y + mon.y_org as i32;
    // info!(
    //     "Got absolute pos {},{} from (x,y) = ({},{}), (x_org, y_org) = ({}, {})",
    //     x_abs, y_abs, x, y, mon.x_org, mon.y_org
    // );
    (x_abs, y_abs)
}
