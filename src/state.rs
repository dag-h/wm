use crate::client::{Client, Geom, Maximized};
use crate::config::Config;
use crate::errors::WMError;
use crate::monitor::{Monitor, MonsIdx, TagNum};
use crate::utils;
use log::{error, info};
use std::collections::{HashMap, HashSet};
use std::io::Write;
use std::process;
use std::process::{Command, Stdio};
use x11rb::atom_manager;
use x11rb::connection::Connection;
use x11rb::protocol::xproto::*;

pub type Result<T> = std::result::Result<T, WMError>;

// grab_button(s) doesn't take a ButtonMask for some reason so this is needed
const BUTTON_PRESS_MASK: u16 = 1 << 2;
const BUTTON_RELEASE_MASK: u16 = 1 << 3;
const POINTER_MOTION_MASK: u16 = 1 << 6;
const BUTTON_MASK: u16 = BUTTON_PRESS_MASK | BUTTON_RELEASE_MASK;
pub const MOUSE_MASK: u16 = BUTTON_MASK | POINTER_MOTION_MASK;
const BORDER_COLOR: u32 = 0xffffff;

atom_manager! {
    pub Atoms: AtomsCookie {
        _NET_WM_STATE,
        _NET_WM_STATE_FULLSCREEN,
        _NET_CLIENT_LIST,
        _NET_ACTIVE_WINDOW,
        _NET_SUPPORTED,
        _NET_SUPPORTING_WM_CHECK,
        _NET_WM_NAME,
        _NET_NUMBER_OF_DESKTOPS,
        _NET_CURRENT_DESKTOP,
        _NET_DESKTOP_NAMES,
        _NET_DESKTOP_VIEWPORT,
        WM_PROTOCOLS,
        WM_DELETE_WINDOW,
        WM_STATE,
    }
}

// a currently active motion of the pointer
pub struct MouseMotion {
    pub x_offs: i16,
    pub y_offs: i16,
    pub quadrant: u8, // which quadrant of the focused window the pointer is in
}

// current state of the wm
pub struct State<C: Connection> {
    pub conn: C,
    screen_num: usize,
    pub clients: Vec<Client>,
    pub mons: Vec<Monitor>,
    pub dragging: Option<MouseMotion>,
    pub last_motion: Timestamp,
    pub motion_button: Option<u8>,
    pub focused_win: Option<Window>,
    pub focused_mon: MonsIdx,
    pub config: Config,
    pub atoms: Atoms,
    pub iconified: HashSet<Window>,
}

impl<C: Connection> State<C> {
    pub fn new(
        conn: C,
        screen_num: usize,
        mons: Vec<Monitor>,
        config: Config,
        atoms: Atoms,
    ) -> Self {
        State {
            conn,
            screen_num,
            clients: Vec::new(),
            mons,
            dragging: None,
            last_motion: 0,
            motion_button: None,
            focused_win: None,
            focused_mon: 0,
            config,
            atoms,
            iconified: HashSet::new(),
        }
    }

    pub fn is_managing(&self, win: Window) -> bool {
        self.clients.iter().any(|client| client.window == win)
    }

    pub fn get_root(&self) -> Window {
        self.conn.setup().roots[self.screen_num].root
    }

    // gets the active tag on the currently focused monitor
    pub fn curr_tag(&self) -> TagNum {
        let mon = get_mon(&self.mons, self.focused_mon);
        mon.tag
    }

    pub fn update_curr_tag(&self) -> Result<()> {
        x11rb::wrapper::ConnectionExt::change_property32(
            &self.conn,
            PropMode::REPLACE,
            self.get_root(),
            self.atoms._NET_CURRENT_DESKTOP,
            AtomEnum::CARDINAL,
            &[self.curr_tag() as u32],
        )?;
        Ok(())
    }
}

// helper function for split borrow of mons field in a State
pub fn get_mon(mons: &[Monitor], mons_idx: MonsIdx) -> &Monitor {
    match mons.get(mons_idx) {
        Some(mon) => mon,
        None => {
            error!("get_mon: index out of range");
            process::exit(1);
        }
    }
}

// helper function for split mutable borrow of mons field in a State
pub fn get_mon_mut(mons: &mut [Monitor], idx: MonsIdx) -> &mut Monitor {
    match mons.get_mut(idx) {
        Some(mon) => mon,
        None => {
            error!("get_mon_mut: index out of range");
            process::exit(1);
        }
    }
}

// helper function for split borrow of clients field in a State
pub fn get_client(clients: &[Client], win: Window) -> Option<&Client> {
    clients.iter().find(|c| c.window == win)
}

// helper function for split mutable borrow of clients field in a State
pub fn get_client_mut(clients: &mut [Client], win: Window) -> Option<&mut Client> {
    clients.iter_mut().find(|c| c.window == win)
}

// true if given coordinates are within the given monitor
pub fn is_on_mon(x: i16, y: i16, mon: &Monitor) -> bool {
    x > mon.x_org
        && x < mon.x_org + mon.width as i16
        && y > mon.y_org
        && y < mon.y_org + mon.height as i16
}

// moves a window using coordinates of the given state's MouseMotion
// and the pointer's current position.
// updates tag of moved client if it crosses monitor boundaries
pub fn move_with_mouse<C: Connection>(
    mut state: State<C>,
    event: MotionNotifyEvent,
) -> Result<State<C>> {
    let offset = match &state.dragging {
        Some(offs) => offs,
        None => {
            error!("move_with_mouse: call to move with mouse without dragging");
            return Ok(state);
        }
    };
    let win = event.event;
    if win == state.get_root() {
        return Ok(state);
    }

    let geom = state.conn.get_geometry(win)?.reply()?;
    let (new_x, new_y) = (
        geom.x + (event.root_x - offset.x_offs),
        geom.y + (event.root_y - offset.y_offs),
    );

    state.conn.configure_window(
        win,
        &ConfigureWindowAux::new().x(new_x as i32).y(new_y as i32),
    )?;

    // getting the clients new relative position
    // TODO might have to move this after setting focused monitor
    let (client_x, client_y) = utils::get_relative_pos(
        get_mon(&state.mons, state.focused_mon),
        new_x as i32,
        new_y as i32,
    );

    // find which monitor the client is on, and get the tag present on that monitor
    // (in order to change tag if client was moved to other monitor)
    let mut tag = get_mon(&state.mons, state.focused_mon).tag;
    for (i, mon) in state.mons.iter().enumerate() {
        // the client is not on the focused monitor anymore, which means it has been moved
        // to another monitor, so we should set its tag to the tag present on that monitor
        // and change currently focused monitor to that one
        // TODO: this is just unnecessary, would be called every time
        if is_on_mon(new_x, new_y, mon) {
            tag = mon.tag;
            state.focused_mon = i;
            state.update_curr_tag()?;
        }
    }

    if let Some(client) = get_client_mut(&mut state.clients, win) {
        client.x = client_x as i16;
        client.y = client_y as i16;
        if client.tag != tag {
            info!("Client moved, setting new tag to {}", tag);
            client.tag = tag;
        }
    } else {
        error!("Tried to get client in move_with_mouse, not found");
        process::exit(1);
    };

    state.conn.flush()?;
    Ok(state)
}

// resizes a window using coordinates of the given state's MouseMotion
// and the pointer's current position.
pub fn resize_with_mouse<C: Connection>(
    mut state: State<C>,
    event: MotionNotifyEvent,
) -> Result<State<C>> {
    let offset = match &state.dragging {
        Some(offs) => offs,
        None => {
            error!("resize_with_mouse: call to resize_with_mouse without dragging");
            return Ok(state);
        }
    };
    let win = event.event;
    if win == state.get_root() {
        return Ok(state);
    }

    // resize should behave differently depending on which quadrant of the window
    // the cursor is in
    let geom = state.conn.get_geometry(win)?.reply()?;

    let mut width_change = event.root_x as i16 - offset.x_offs as i16;
    let mut height_change = event.root_y as i16 - offset.y_offs as i16;

    let mut cfg = ConfigureWindowAux::new();

    if offset.quadrant == 2 || offset.quadrant == 3 {
        width_change *= -1;
        cfg = cfg.x(geom.x as i32 - width_change as i32);
    }
    if offset.quadrant == 1 || offset.quadrant == 2 {
        height_change *= -1;
        cfg = cfg.y(geom.y as i32 - height_change as i32);
    }

    let mut new_width = geom.width as i16 + width_change;
    let mut new_height = geom.height as i16 + height_change;
    if new_width < 0 || new_height < 0 {
        new_width = 0;
        new_height = 0;
    }

    cfg = cfg.width(new_width as u32).height(new_height as u32);

    state.conn.configure_window(win, &cfg)?;

    let geom = state.conn.get_geometry(win)?.reply()?;
    let mon = get_mon(&state.mons, state.focused_mon);
    let (x, y) = utils::get_relative_pos(mon, geom.x as i32, geom.y as i32);
    if let Some(client) = get_client_mut(&mut state.clients, win) {
        client.width = new_width as u16;
        client.height = new_height as u16;
        client.x = x as i16;
        client.y = y as i16;
    } else {
        error!("resize_with_mouse: client could not be found");
        return Ok(state);
    };
    state.conn.flush()?;
    Ok(state)
}

// grabs input for all buttons present in config with their respective modifiers
fn grab_buttons<C: Connection>(state: State<C>, win: Window) -> Result<State<C>> {
    for b in state.config.bindings.iter() {
        // currently considering codes 8 and under to be mouse buttons, but this leaves
        // us not being able to bind button 9, and also not being able to bind key 8
        // (which is escape on my system)
        if b.keycode <= 8 {
            state.conn.grab_button(
                false,
                win,
                BUTTON_MASK,
                GrabMode::ASYNC,
                GrabMode::ASYNC,
                0_u32,
                0_u32,
                ButtonIndex::from(b.keycode),
                b.modifier,
            )?;
        }
    }
    Ok(state)
}

pub fn manage_window<C: Connection>(
    mut state: State<C>,
    window: Window,
    geom: GetGeometryReply,
) -> Result<State<C>> {
    if state.is_managing(window) {
        info!("Tried to manage already managed window: {}", window);
        return Ok(state);
    }

    // don't manage windows with override-redirect
    let attr = state.conn.get_window_attributes(window)?.reply()?;
    if attr.override_redirect {
        info!("Ignoring window (override-redirect): {}", window);
        state.conn.map_window(window)?;
        return Ok(state);
    }

    let border_width = state.config.border_width;
    let mon = get_mon(&state.mons, state.focused_mon);

    // get centered window placement on the current monitor
    let (x, y) = if geom.width >= mon.width || geom.height >= mon.height {
        info!("Window too large, setting pos at origin");
        (mon.x_org as i32, mon.y_org as i32)
    } else {
        let x: i32 = mon.x_org as i32 + mon.width as i32 / 2 - geom.width as i32 / 2;
        let y: i32 = mon.y_org as i32 + mon.height as i32 / 2 - geom.height as i32 / 2;
        (x, y)
    };

    let cfg = ConfigureWindowAux::new()
        .border_width(border_width)
        .x(x)
        .y(y);
    let attr = ChangeWindowAttributesAux::new()
        .border_pixel(BORDER_COLOR)
        .event_mask(
            EventMask::ENTER_WINDOW
                | EventMask::FOCUS_CHANGE
                | EventMask::PROPERTY_CHANGE
                | EventMask::STRUCTURE_NOTIFY,
        );

    state.conn.change_window_attributes(window, &attr)?;
    state.conn.configure_window(window, &cfg)?;

    // self.conn.change_save_set(SetMode::INSERT, window)?;

    state.conn.map_window(window)?;

    // move pointer to center (which will also be the center of the new window)
    state.conn.warp_pointer(
        0_u32,
        state.get_root(),
        0_i16,
        0_i16,
        0_u16,
        0_u16,
        mon.x_org + mon.width as i16 / 2,
        mon.y_org + mon.height as i16 / 2,
    )?;

    // get updated geometry
    let geom = state.conn.get_geometry(window)?.reply()?;
    let mut client = Client::new(window, geom, mon);

    client.tag = state.curr_tag();

    state.clients.push(client);

    state = ewmh_update_client_list(state)?;

    info!("Successfully managed window: {}", window);

    // let (x, y) = match get_free_placement(&state, window) {
    //     Some((x, y)) => (x, y),
    //     None => {
    //         error!("manage_window: could not find free placement");
    //         return Ok(state);
    //     }
    // };
    // state = move_win(state, window, x as i32, y as i32)?;
    // let cfg = ConfigureWindowAux::new().x(x as i32).y(y as i32);
    // state.conn.configure_window(window, &cfg)?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        window,
        state.atoms.WM_STATE,
        state.atoms.WM_STATE,
        &[1],
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        window,
        state.atoms._NET_WM_STATE,
        AtomEnum::ATOM,
        &[],
    )?;

    Ok(state)
}

// EWMH requires wm to keep an updated list of managed windows on the root window
fn ewmh_update_client_list<C: Connection>(state: State<C>) -> Result<State<C>> {
    let wins = state
        .clients
        .iter()
        .map(|client| client.window)
        .collect::<Vec<_>>();
    let atom = state.atoms._NET_CLIENT_LIST;
    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        atom,
        AtomEnum::WINDOW,
        wins.as_slice(),
    )?;
    Ok(state)
}

// manages and maps all unmanaged windows, used at startup
pub fn map_unmapped_windows<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    let reply = state.conn.query_tree(state.get_root())?.reply()?;
    let wins = reply
        .children
        .iter()
        .filter(|win| !state.is_managing(**win))
        .collect::<Vec<&Window>>();
    for win in wins {
        let geom = state.conn.get_geometry(*win)?.reply()?;
        state = manage_window(state, *win, geom)?;
    }
    Ok(state)
}

pub fn spawn(s: &str) -> Result<()> {
    match Command::new("sh").arg("-c").arg(s).spawn() {
        Ok(_) => Ok(()),
        Err(err) => Err(err.into()),
    }
}

// sends a ClientMessage to the focused window or the given window if it is Some, requesting
// it to close itself
pub fn kill_win<C: Connection>(mut state: State<C>, win: Option<Window>) -> Result<State<C>> {
    let win = match win {
        Some(w) => Some(w),
        None => state.focused_win,
    };
    if let Some(win) = win {
        // check that we are actually managing the window
        if state.is_managing(win) {
            let to_send = ClientMessageEvent {
                response_type: CLIENT_MESSAGE_EVENT,
                format: 32,
                sequence: 0,
                window: win,
                type_: state.atoms.WM_PROTOCOLS,
                data: [state.atoms.WM_DELETE_WINDOW, 0, 0, 0, 0].into(),
            };
            state
                .conn
                .send_event(false, win, EventMask::NO_EVENT, &to_send)?
                .check()?;
            state = focus_next(state)?;
        }
    }
    Ok(state)
}

// unmanages the window by removing it from the given state's clients
pub fn unmanage<C: Connection>(mut state: State<C>, win: Window) -> State<C> {
    if let Some(idx) = state.clients.iter().position(|c| c.window == win) {
        state.clients.remove(idx);
    }
    state
}

// activates the given tag by either focusing a monitor with the tag present, or unmapping
// all currently focused clients and mapping the clients of that tag
pub fn goto_tag<C: Connection>(mut state: State<C>, tag: TagNum) -> Result<State<C>> {
    info!("Going to tag {}", tag);

    if tag == state.curr_tag() {
        info!("Already on tag");
        return Ok(state);
    }

    let mons = &state.mons;
    // if target tag is present on another monitor, just focus the monitor
    for (i, mon) in mons.iter().enumerate() {
        if mon.tag == tag {
            info!("Tag on other monitor, switching focus");
            state.focused_mon = i;
            // move pointer to center of target monitor
            state.conn.warp_pointer(
                0_u32,
                state.get_root(),
                0_i16,
                0_i16,
                0_u16,
                0_u16,
                mon.x_org + mon.width as i16 / 2,
                mon.y_org + mon.height as i16 / 2,
            )?;
            state = focus_next(state)?;
            state.update_curr_tag()?;
            return Ok(state);
        }
    }

    let curr_tag = state.curr_tag();
    let visible_wins = state
        .clients
        .iter()
        .filter(|cl| cl.tag == curr_tag)
        .map(|cl| cl.window);
    let wins_to_map = state
        .clients
        .iter()
        .filter(|cl| cl.tag == tag)
        .map(|cl| cl.window)
        .collect::<Vec<_>>();

    // otherwise we will go to the target tag on the focused monitor, so we need
    // to unmap all windows of the current tag and map all windows with the target tag
    for win in visible_wins {
        state.conn.unmap_window(win)?;
    }
    for win in wins_to_map {
        // update_win(&state, cl)?;
        state = update_win(state, win, None)?;
        let client = if let Some(client) = get_client(&state.clients, win) {
            client
        } else {
            error!("goto_tag: could not find client");
            continue;
        };
        if !client.iconified {
            state.conn.map_window(win)?;
            state.conn.flush()?; // TODO needed?
        }
    }

    let mon = get_mon_mut(&mut state.mons, state.focused_mon);
    mon.tag = tag;

    state = focus_next(state)?;

    state.update_curr_tag()?;

    Ok(state)
}

// sets the tag on the focused/given window to the given tag
pub fn set_tag<C: Connection>(
    mut state: State<C>,
    tag: TagNum,
    win: Option<Window>,
) -> Result<State<C>> {
    let win = match win {
        Some(win) => win,
        None => match state.focused_win {
            Some(win) => win,
            None => return Ok(state),
        },
    };
    if let Some(client) = get_client_mut(&mut state.clients, win) {
        info!("Setting tag to {}", tag);
        client.tag = tag;
        // if the given tag is present on another monitor, we need to update the monitor
        // to show the client
        state = update_mons(state)?;
    }
    Ok(state)
}

// maps any clients with any of the active tags, and unmaps all other clients
fn update_mons<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    let active_tags = state.mons.iter().map(|mon| mon.tag).collect::<Vec<_>>();

    let mut active_clients = HashMap::new();
    let mut inactive_clients = Vec::new();
    'outer: for client in state.clients.iter() {
        for tag in active_tags.iter() {
            info!("Client tag: {}, tag: {}", client.tag, tag);
            if *tag == client.tag {
                info!("Found active client");
                active_clients.insert(client.window, client.tag);
                continue 'outer;
            }
        }
        inactive_clients.push(client);
    }

    for cl in inactive_clients {
        info!("Unmapping inactive client: {}", cl.window);
        state.conn.unmap_window(cl.window)?;
    }

    for cl in active_clients.iter() {
        let mon_idx = if let Some(mon) = state.mons.iter().find(|m| *cl.1 == m.tag) {
            mon.idx
        } else {
            info!("Active client but no monitor found with active tag");
            get_mon(&state.mons, state.focused_mon).idx
        };
        // update_win_in_mon(&state, cl, mon)?;
        state = update_win(state, *cl.0, Some(mon_idx))?;
        let client = if let Some(client) = get_client(&state.clients, *cl.0) {
            client
        } else {
            error!("update_mons: could not find client");
            continue;
        };
        if !client.iconified {
            state.conn.map_window(*cl.0)?;
        }
    }
    Ok(state)
}

// updates the given client's absolute position based on its relative
// position within the currently focused monitor
fn update_win<C: Connection>(
    mut state: State<C>,
    win: Window,
    mon_idx: Option<MonsIdx>,
) -> Result<State<C>> {
    let (maximized, win) = if let Some(client) = get_client(&state.clients, win) {
        (client.maximized, client.window)
    } else {
        error!("update_win: could not find client");
        return Ok(state);
    };

    state = set_win_state(state, maximized, win, mon_idx)?;
    // let state = match maximized {
    //     Maximized::None => set_win_state(state, Maximized::None, win, mon_idx)?,
    //     Maximized::Full | Maximized::FromEvent => {
    //         set_win_state(state, Maximized::Full, win, mon_idx)?
    //     }
    //     Maximized::Left => set_win_state(state, Maximized::Left, win, mon_idx)?,
    //     Maximized::Right => set_win_state(state, Maximized::Right, win, mon_idx)?,
    // };
    Ok(state)
}

pub fn maximize_toggle<C: Connection>(
    mut state: State<C>,
    mode: Maximized,
    opt_win: Option<Window>,
) -> Result<State<C>> {
    let win = match opt_win {
        Some(win) => win,
        None => match state.focused_win {
            Some(win) => win,
            None => return Ok(state),
        },
    };

    let maximized = if let Some(client) = get_client(&state.clients, win) {
        client.maximized
    } else {
        return Ok(state);
    };

    match mode {
        Maximized::Left => match maximized {
            Maximized::Left => {
                state = set_win_state(state, Maximized::None, win, None)?;
            }
            _ => state = set_win_state(state, Maximized::Left, win, None)?,
        },
        Maximized::Right => match maximized {
            Maximized::Right => {
                state = set_win_state(state, Maximized::None, win, None)?;
            }
            _ => state = set_win_state(state, Maximized::Right, win, None)?,
        },
        Maximized::Full => match maximized {
            Maximized::Full => {
                state = set_win_state(state, Maximized::None, win, None)?;
            }
            _ => state = set_win_state(state, Maximized::Full, win, None)?,
        },
        _ => return Ok(state),
    }

    Ok(state)
}

pub fn set_win_state<C: Connection>(
    mut state: State<C>,
    mode: Maximized,
    win: Window,
    mon_idx: Option<MonsIdx>,
) -> Result<State<C>> {
    let client = if let Some(client) = get_client_mut(&mut state.clients, win) {
        client
    } else {
        return Ok(state);
    };
    let mon = if let Some(idx) = mon_idx {
        get_mon(&state.mons, idx)
    } else {
        get_mon(&state.mons, state.focused_mon)
    };

    let border_width = state.config.border_width;
    let width: u32;
    let height: u32;
    let x: i32;
    let y: i32;
    let bw: u32;
    let mut cfg = ConfigureWindowAux::new();

    match mode {
        Maximized::Left => {
            // client.maximized = Maximized::Left;
            width = mon.width as u32 / 2 - 2 * border_width;
            height = mon.height as u32 - 2 * border_width;
            x = mon.x_org as i32;
            y = mon.y_org as i32;
            bw = border_width;
            cfg = cfg.stack_mode(StackMode::ABOVE);
        }
        Maximized::Right => {
            // client.maximized = Maximized::Right;
            width = mon.width as u32 / 2 - 2 * border_width;
            height = mon.height as u32 - 2 * border_width;
            x = mon.x_org as i32 + mon.width as i32 / 2;
            y = mon.y_org as i32;
            bw = border_width;
            cfg = cfg.stack_mode(StackMode::ABOVE);
        }
        Maximized::Full => {
            // client.maximized = Maximized::Full;
            width = mon.width as u32;
            height = mon.height as u32;
            x = mon.x_org as i32;
            y = mon.y_org as i32;
            bw = 0;
            cfg = cfg.stack_mode(StackMode::ABOVE);
            x11rb::wrapper::ConnectionExt::change_property32(
                &state.conn,
                PropMode::REPLACE,
                win,
                state.atoms._NET_WM_STATE,
                AtomEnum::ATOM,
                &[state.atoms._NET_WM_STATE_FULLSCREEN],
            )?;
        }
        Maximized::None => {
            if let Maximized::None = client.maximized {
                let (x_abs, y_abs) = utils::get_absolute_pos(mon, client.x as i32, client.y as i32);
                width = client.width as u32;
                height = client.height as u32;
                x = x_abs;
                y = y_abs;
            } else {
                // client.maximized = Maximized::None;
                let (x_abs, y_abs) = utils::get_absolute_pos(
                    mon,
                    client.old_geom.x as i32,
                    client.old_geom.y as i32,
                );
                width = client.old_geom.width as u32;
                height = client.old_geom.height as u32;
                x = x_abs;
                y = y_abs;
            }
            x11rb::wrapper::ConnectionExt::change_property32(
                &state.conn,
                PropMode::REPLACE,
                win,
                state.atoms._NET_WM_STATE,
                AtomEnum::ATOM,
                &[],
            )?;
            bw = border_width;
        }
        Maximized::FromEvent => {
            // client.maximized = Maximized::FromEvent;
            width = mon.width as u32;
            height = mon.height as u32;
            x = mon.x_org as i32;
            y = mon.y_org as i32;
            bw = 0;
            cfg = cfg.stack_mode(StackMode::ABOVE);
            x11rb::wrapper::ConnectionExt::change_property32(
                &state.conn,
                PropMode::REPLACE,
                win,
                state.atoms._NET_WM_STATE,
                AtomEnum::ATOM,
                &[state.atoms._NET_WM_STATE_FULLSCREEN],
            )?;
        }
    }

    // only update the old geometry if we actually changed into a new mode
    // (so not on regular window updates from update_win)
    if mode != client.maximized {
        client.old_geom = Geom::new(client.x, client.y, client.width, client.height);
    }
    client.maximized = mode;

    // update client state
    // let old_geom = Geom::new(client.x, client.y, client.width, client.height);
    // client.old_geom = old_geom;
    client.width = width as u16;
    client.height = height as u16;
    let (x_rel, y_rel) = utils::get_relative_pos(mon, x, y);
    client.x = x_rel as i16;
    client.y = y_rel as i16;

    // apply changes to client
    cfg = cfg.border_width(bw).width(width).height(height).x(x).y(y);
    state.conn.configure_window(client.window, &cfg)?;
    state.conn.flush()?;
    Ok(state)
}

pub fn autostart<C: Connection>(state: &State<C>) -> Result<()> {
    for cmd in state.config.autostart.iter() {
        spawn(cmd)?;
    }
    Ok(())
}

pub fn switchmons<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    if state.mons.len() == 1 {
        return Ok(state);
    }
    let curr_mon = get_mon_mut(&mut state.mons, state.focused_mon);
    let curr_mon_idx = curr_mon.idx;
    let curr_mon_tag = curr_mon.tag;

    let mut next_mon;
    if curr_mon_idx == state.mons.len() - 1 {
        next_mon = get_mon_mut(&mut state.mons, 0);
    } else {
        next_mon = get_mon_mut(&mut state.mons, curr_mon_idx + 1);
    }

    let next_mon_tag = next_mon.tag;
    next_mon.tag = curr_mon_tag;

    let curr_mon = get_mon_mut(&mut state.mons, state.focused_mon);
    curr_mon.tag = next_mon_tag;
    state = update_mons(state)?;
    state.update_curr_tag()?;
    Ok(state)
}

pub fn focus_next<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    if let Some(win) = state.focused_win {
        // check if the focused window is on our current tag, and dont try to cycle if it's
        // not. instead, find a client on the current tag and focus that
        if let Some(client) = get_client(&state.clients, win) {
            let tag = state.curr_tag();
            if client.tag != tag {
                let mut matched_win = None;
                if let Some(client) = state
                    .clients
                    .iter()
                    .find(|cl| cl.tag == tag && !cl.iconified)
                {
                    matched_win = Some(client.window);
                }
                // TODO: this could be done directly in the if-statement above?
                if let Some(win) = matched_win {
                    state = focus(state, win)?;
                    state = warp_to_win(state, win)?;
                    return Ok(state);
                }
            }
        }

        let clients_on_mon = state
            .clients
            .iter()
            .filter(|cl| cl.tag == state.curr_tag() && !cl.iconified)
            .map(|cl| cl.window)
            .collect::<Vec<_>>();

        if clients_on_mon.is_empty() {
            state.focused_win = None;
            return Ok(state);
        }
        if let Some(pos) = clients_on_mon.iter().position(|w| *w == win) {
            let mut clients_on_mon = clients_on_mon.iter().cycle().skip(pos + 1);
            // unwrap is ok here since we have already asserted that clients_on_mon has
            // elements and we have a cyclic iterator
            let next_win = clients_on_mon.next().unwrap();
            state = focus(state, *next_win)?;
            state = warp_to_win(state, *next_win)?;
            state = raise_win(state, *next_win)?;
        } else {
            error!("focus_next: Focused window could not be found on focused monitor");
            return Ok(state);
        }
    } else {
        // if we have no focused window, find a client on the current tag to focus on
        let tag = state.curr_tag();
        let mut matched_win = None;
        if let Some(client) = state
            .clients
            .iter()
            .find(|cl| cl.tag == tag && !cl.iconified)
        {
            matched_win = Some(client.window);
        }
        if let Some(win) = matched_win {
            state = focus(state, win)?;
            state = warp_to_win(state, win)?;
        }
    }
    Ok(state)
}

pub fn focus<C: Connection>(mut state: State<C>, win: Window) -> Result<State<C>> {
    state
        .conn
        .set_input_focus(InputFocus::POINTER_ROOT, win, 0_u32)?;
    state.conn.change_property(
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_ACTIVE_WINDOW,
        AtomEnum::WINDOW,
        32,
        1,
        &win.to_ne_bytes(),
    )?;
    state.focused_win = Some(win);
    state = grab_buttons(state, win)?;
    if let Some(client) = get_client(&state.clients, win) {
        for mon in state.mons.iter() {
            if mon.tag == client.tag {
                state.focused_mon = mon.idx;
                state.update_curr_tag()?;
                return Ok(state);
            }
        }
    } else {
        error!("focus: tried to focus window but could not find client");
    }
    Ok(state)
}

pub fn center_win<C: Connection>(state: State<C>) -> Result<State<C>> {
    let client = if let Some(win) = state.focused_win {
        if let Some(client) = get_client(&state.clients, win) {
            client
        } else {
            error!("center_win: tried to center focused window but could not find managed client");
            return Ok(state);
        }
    } else {
        return Ok(state);
    };
    let win = client.window;
    let mon = get_mon(&state.mons, state.focused_mon);
    let (x, y) = utils::get_centered_placement(mon, client);
    move_win(state, Some(win), x, y, MoveType::Abs)
}

pub enum MoveType {
    Inc,
    Abs,
}

// depending on MoveType, x and y will either be absolute positions to move window to or an amount
// to move with
pub fn move_win<C: Connection>(
    mut state: State<C>,
    win: Option<Window>,
    x: i32,
    y: i32,
    mode: MoveType,
) -> Result<State<C>> {
    let win = match win {
        Some(win) => win,
        None => match state.focused_win {
            Some(win) => win,
            None => return Ok(state),
        },
    };
    if let Some(client) = get_client_mut(&mut state.clients, win) {
        // fullscreen clients can't be resized/moved
        match client.maximized {
            Maximized::Full => return Ok(state),
            Maximized::None => (),
            _ => client.maximized = Maximized::None,
        }

        let mut cfg = ConfigureWindowAux::new();
        match mode {
            MoveType::Abs => {
                cfg = cfg.x(x).y(y);
                let (x, y) = utils::get_relative_pos(get_mon(&state.mons, state.focused_mon), x, y);
                client.x = x as i16;
                client.y = y as i16;
            }
            MoveType::Inc => {
                let (curr_x, curr_y) = utils::get_absolute_pos(
                    get_mon(&state.mons, state.focused_mon),
                    client.x as i32,
                    client.y as i32,
                );
                let new_x = curr_x + x as i32;
                let new_y = curr_y + y as i32;
                cfg = cfg.x(new_x).y(new_y);
                client.x += x as i16;
                client.y += y as i16;
                for (i, mon) in state.mons.iter().enumerate() {
                    if is_on_mon(new_x as i16, new_y as i16, mon) {
                        state.focused_mon = i;
                        client.tag = mon.tag;
                        let (x, y) = utils::get_relative_pos(mon, new_x, new_y);
                        client.x = x as i16;
                        client.y = y as i16;
                    }
                }
            }
        }
        state = warp_to_win(state, win)?;
        state.conn.configure_window(win, &cfg)?;
        state.update_curr_tag()?;
    }
    Ok(state)
}

fn raise_win<C: Connection>(state: State<C>, win: Window) -> Result<State<C>> {
    state
        .conn
        .configure_window(win, &ConfigureWindowAux::new().stack_mode(StackMode::ABOVE))?;
    Ok(state)
}

fn warp_to_win<C: Connection>(mut state: State<C>, win: Window) -> Result<State<C>> {
    let client = if let Some(client) = get_client_mut(&mut state.clients, win) {
        client
    } else {
        return Ok(state);
    };
    let width = client.width;
    let height = client.height;
    let (x, y) = utils::get_absolute_pos(
        get_mon(&state.mons, state.focused_mon),
        client.x as i32,
        client.y as i32,
    );
    state.conn.warp_pointer(
        0_u32,
        state.get_root(),
        0_i16,
        0_i16,
        0_u16,
        0_u16,
        x as i16 + width as i16 / 2,
        y as i16 + height as i16 / 2,
    )?;
    Ok(state)
}

pub fn iconify<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    let win = match state.focused_win {
        Some(win) => win,
        None => return Ok(state),
    };
    // focusing next after saving the currently focused window
    state = focus_next(state)?;
    let client = if let Some(client) = get_client_mut(&mut state.clients, win) {
        client
    } else {
        error!("uniconify: could not find client");
        return Ok(state);
    };
    client.iconified = true;
    state.conn.unmap_window(win)?;
    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        win,
        state.atoms.WM_STATE,
        state.atoms.WM_STATE,
        &[3],
    )?;

    Ok(state)
}

pub fn uniconify<C: Connection>(mut state: State<C>) -> Result<State<C>> {
    let wins = &state.iconified;

    if wins.is_empty() {
        return Ok(state);
    }

    // creating map of (window title, window id)
    let mut titles: HashMap<String, Window> = HashMap::new();
    for win in wins {
        let prop = state
            .conn
            .get_property(
                false,
                *win,
                state.atoms._NET_WM_NAME,
                AtomEnum::ANY,
                0,
                u32::MAX,
            )?
            .reply()?;
        let title = String::from_utf8_lossy(&prop.value).trim().to_string();
        titles.insert(title, *win);
    }

    info!("titles:\n{:#?}", titles);

    // formatting input for rofi
    let mut entries = String::new();
    for (i, title) in titles.keys().enumerate() {
        entries.push_str(&title.to_string());
        if i + 1 != titles.len() {
            entries.push('\n');
        }
    }

    let mut child = Command::new("rofi")
        .arg("-dmenu")
        .arg("-i")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    match child.stdin.as_mut() {
        Some(stdin) => {
            stdin.write_all(entries.as_bytes())?;
        }
        None => {
            error!("uniconify: could not capture stdin");
            return Ok(state);
        }
    }

    // get stdout from rofi, which would be the chosen window to uniconify
    let output = child.wait_with_output();

    let output = match output {
        Ok(out) => out,
        Err(err) => {
            error!("error getting output: {}", err);
            return Ok(state);
        }
    };
    let win_str = String::from_utf8_lossy(&output.stdout).trim().to_string();
    info!("win_str: {}", win_str);
    let win = if let Some(win) = titles.get(win_str.trim()) {
        win
    } else {
        error!("uniconfify: could not find window in titles hashmap");
        return Ok(state);
    };

    if state.iconified.contains(win) {
        let tag = state.curr_tag();
        state = set_tag(state, tag, Some(*win))?;
        state.conn.map_window(*win)?;
        state = focus(state, *win)?;
        state.conn.configure_window(
            *win,
            &ConfigureWindowAux::new().stack_mode(StackMode::ABOVE),
        )?;
    }
    Ok(state)
}

#[derive(Clone, Copy)]
pub enum Dir {
    Next,
    Prev,
}

// goes to the next tag in order, skipping tags present on other monitors
pub fn cycle_tags<C: Connection>(state: State<C>, dir: Dir) -> Result<State<C>> {
    let max_tag = 8;
    let mut tags = (0_usize..=max_tag).collect::<Vec<_>>();
    let curr_tag = state.curr_tag();
    let active_tags = state.mons.iter().map(|mon| mon.tag).collect::<HashSet<_>>();
    match dir {
        Dir::Next => (),
        Dir::Prev => tags.reverse(),
    }
    // unwrapping here since we know curr_tag will always be in the range 0..=max_tag
    let pos = tags.iter().position(|t| *t == curr_tag).unwrap();
    let tags = tags.iter().cycle().skip(pos);
    let mut next_tag = curr_tag;
    for tag in tags {
        if active_tags.contains(tag) {
            continue;
        } else {
            next_tag = *tag;
            break;
        }
    }
    goto_tag(state, next_tag)
}

fn _get_free_placement<C: Connection>(state: &State<C>, win: Window) -> Option<(i16, i16)> {
    let mut taken: Vec<((i16, i16), (i16, i16))> = Vec::new();
    let client = match get_client(&state.clients, win) {
        Some(client) => client,
        None => {
            error!("get_free_placement: could not find client");
            return None;
        }
    };
    for cl in state.clients.iter() {
        if cl.window == client.window {
            continue;
        }
        let spans = (
            (cl.x, cl.y),
            (cl.x + cl.width as i16, cl.y + cl.height as i16),
        );
        taken.push(spans);
    }

    info!("spans: {:?}", taken);

    let mon = get_mon(&state.mons, state.focused_mon);

    let xs = (10..mon.width as i16 - client.width as i16).collect::<Vec<i16>>();
    let ys = (10..mon.height as i16 - client.height as i16).collect::<Vec<i16>>();
    for y in ys.chunks(20) {
        'x_loop: for x in xs.chunks(20) {
            let x = x[0];
            let y = y[0];
            let mut overlapping = false;
            for span in taken.iter() {
                // let overlapping;
                let l1 = (x, y);
                let r1 = (x + client.width as i16, y + client.height as i16);
                let l2 = span.0;
                let r2 = span.1;
                if r1.0 < l2.0 || r2.0 < l1.0 {
                    info!(
                        "Not overlapping, left of:\nl1: {:?}\nr1: {:?}\nl2: {:?}\nr2: {:?}\n",
                        l1, r1, l2, r2
                    );
                    overlapping = false;
                } else if r1.1 < l2.1 || r2.1 < l1.1 {
                    info!(
                        "Not overlapping, above:\nl1: {:?}\nr1: {:?}\nl2: {:?}\nr2: {:?}\n",
                        l1, r1, l2, r2
                    );
                    overlapping = false;
                } else {
                    overlapping = true;
                }

                if overlapping {
                    continue 'x_loop;
                }
            }
            if !overlapping {
                info!("found placement: ({}, {})", x, y);
                return Some((x, y));
            }
        }
    }
    None
}

pub fn resize_win<C: Connection>(mut state: State<C>, x: i32, y: i32) -> Result<State<C>> {
    let win = if let Some(win) = state.focused_win {
        win
    } else {
        return Ok(state);
    };
    if let Some(client) = get_client_mut(&mut state.clients, win) {
        // fullscreen clients can be resized/moved
        match client.maximized {
            Maximized::Full => return Ok(state),
            Maximized::None => (),
            _ => client.maximized = Maximized::None,
        }
        let mut new_width = client.width as i32 + x;
        let mut new_height = client.height as i32 + y;

        if new_height < 0 {
            new_height = 0;
        }
        if new_width < 0 {
            new_width = 0;
        }

        let cfg = ConfigureWindowAux::new()
            .width(new_width as u32)
            .height(new_height as u32);

        state.conn.configure_window(win, &cfg)?;

        client.width = new_width as u16;
        client.height = new_height as u16;
        state = warp_to_win(state, win)?;
    }
    Ok(state)
}
