use crate::state::Dir;
use x11rb::protocol::xproto::ModMask;
use Action::*;

#[allow(unused)]
impl Config {
    pub fn default() -> Self {
        let mod1: ModMask = ModMask::M1;
        let mod2: ModMask = ModMask::M2;
        let mod3: ModMask = ModMask::M3;
        let mod4: ModMask = ModMask::M4;
        let mod5: ModMask = ModMask::M5;
        let shift: ModMask = ModMask::SHIFT;
        let ctrl: ModMask = ModMask::CONTROL;
        let none: ModMask = ModMask::from(0_u8);
        const MOVE_RESIZE_AMOUNT: i32 = 35;

        let modkey = mod1;
        Config {
            bindings: vec![
                Binding {
                    keycode: 40, // D
                    modifier: modkey,
                    action: Spawn("rofi -show run"),
                },
                Binding {
                    keycode: 36, // Return
                    modifier: modkey,
                    action: Spawn("kitty"),
                },
                Binding {
                    keycode: 232, // some extra function key
                    modifier: none,
                    action: Spawn("xbacklight -dec 10"),
                },
                Binding {
                    keycode: 233, // some extra function key
                    modifier: none,
                    action: Spawn("xbacklight -inc 10"),
                },
                Binding {
                    keycode: 121, // some extra function key
                    modifier: none,
                    action: Spawn("pulseaudio-ctl mute"),
                },
                Binding {
                    keycode: 122, // some extra function key
                    modifier: none,
                    action: Spawn("pulseaudio-ctl down"),
                },
                Binding {
                    keycode: 123, // some extra function key
                    modifier: none,
                    action: Spawn("pulseaudio-ctl up"),
                },
                Binding {
                    keycode: 39, // S
                    modifier: modkey | shift,
                    action: Spawn("switchkeys"),
                },
                Binding {
                    keycode: 43, // H
                    modifier: modkey | shift,
                    action: ResizeWin(-MOVE_RESIZE_AMOUNT, 0),
                },
                Binding {
                    keycode: 44, // J
                    modifier: modkey | shift,
                    action: ResizeWin(0, MOVE_RESIZE_AMOUNT),
                },
                Binding {
                    keycode: 45, // K
                    modifier: modkey | shift,
                    action: ResizeWin(0, -MOVE_RESIZE_AMOUNT),
                },
                Binding {
                    keycode: 46, // L
                    modifier: modkey | shift,
                    action: ResizeWin(MOVE_RESIZE_AMOUNT, 0),
                },
                Binding {
                    keycode: 57,
                    modifier: modkey | shift,
                    action: ResizeWin(-MOVE_RESIZE_AMOUNT, -MOVE_RESIZE_AMOUNT),
                },
                Binding {
                    keycode: 58,
                    modifier: modkey | shift,
                    action: ResizeWin(MOVE_RESIZE_AMOUNT, MOVE_RESIZE_AMOUNT),
                },
                Binding {
                    keycode: 43,
                    modifier: modkey,
                    action: MoveWindow(MoveResizeType::Value(-MOVE_RESIZE_AMOUNT, 0)),
                },
                Binding {
                    keycode: 44,
                    modifier: modkey,
                    action: MoveWindow(MoveResizeType::Value(0, MOVE_RESIZE_AMOUNT)),
                },
                Binding {
                    keycode: 45,
                    modifier: modkey,
                    action: MoveWindow(MoveResizeType::Value(0, -MOVE_RESIZE_AMOUNT)),
                },
                Binding {
                    keycode: 46,
                    modifier: modkey,
                    action: MoveWindow(MoveResizeType::Value(MOVE_RESIZE_AMOUNT, 0)),
                },
                Binding {
                    keycode: 24,
                    modifier: modkey,
                    action: CloseWindow,
                },
                Binding {
                    keycode: 41,
                    modifier: modkey,
                    action: FullscreenToggle,
                },
                Binding {
                    keycode: 23,
                    modifier: modkey,
                    action: SwitchMons,
                },
                Binding {
                    keycode: 65,
                    modifier: modkey,
                    action: FocusNext,
                },
                Binding {
                    keycode: 54,
                    modifier: modkey,
                    action: CenterWin,
                },
                Binding {
                    keycode: 24,
                    modifier: modkey | shift,
                    action: Quit,
                },
                Binding {
                    keycode: 30,
                    modifier: modkey,
                    action: Uniconify,
                },
                Binding {
                    keycode: 31,
                    modifier: modkey,
                    action: Iconify,
                },
                Binding {
                    keycode: 59,
                    modifier: modkey,
                    action: CycleTags(Dir::Prev),
                },
                Binding {
                    keycode: 60,
                    modifier: modkey,
                    action: CycleTags(Dir::Next),
                },
                Binding {
                    keycode: 32,
                    modifier: modkey | shift,
                    action: Maximize(MaximizeSide::Left),
                },
                Binding {
                    keycode: 33,
                    modifier: modkey | shift,
                    action: Maximize(MaximizeSide::Right),
                },
                Binding {
                    keycode: 1,
                    modifier: modkey,
                    action: MoveWindow(MoveResizeType::Mouse),
                },
                Binding {
                    keycode: 3,
                    modifier: modkey,
                    action: ResizeWindow(MoveResizeType::Mouse),
                },
                Binding {
                    keycode: 10,
                    modifier: modkey,
                    action: GotoTag(1),
                },
                Binding {
                    keycode: 11,
                    modifier: modkey,
                    action: GotoTag(2),
                },
                Binding {
                    keycode: 12,
                    modifier: modkey,
                    action: GotoTag(3),
                },
                Binding {
                    keycode: 13,
                    modifier: modkey,
                    action: GotoTag(4),
                },
                Binding {
                    keycode: 14,
                    modifier: modkey,
                    action: GotoTag(5),
                },
                Binding {
                    keycode: 15,
                    modifier: modkey,
                    action: GotoTag(6),
                },
                Binding {
                    keycode: 16,
                    modifier: modkey,
                    action: GotoTag(7),
                },
                Binding {
                    keycode: 17,
                    modifier: modkey,
                    action: GotoTag(8),
                },
                Binding {
                    keycode: 18,
                    modifier: modkey,
                    action: GotoTag(9),
                },
                Binding {
                    keycode: 10,
                    modifier: modkey | shift,
                    action: SetTag(1),
                },
                Binding {
                    keycode: 11,
                    modifier: modkey | shift,
                    action: SetTag(2),
                },
                Binding {
                    keycode: 12,
                    modifier: modkey | shift,
                    action: SetTag(3),
                },
                Binding {
                    keycode: 13,
                    modifier: modkey | shift,
                    action: SetTag(4),
                },
                Binding {
                    keycode: 14,
                    modifier: modkey | shift,
                    action: SetTag(5),
                },
                Binding {
                    keycode: 15,
                    modifier: modkey | shift,
                    action: SetTag(6),
                },
                Binding {
                    keycode: 16,
                    modifier: modkey | shift,
                    action: SetTag(7),
                },
                Binding {
                    keycode: 17,
                    modifier: modkey | shift,
                    action: SetTag(8),
                },
                Binding {
                    keycode: 18,
                    modifier: modkey | shift,
                    action: SetTag(9),
                },
            ],
            border_width: 0,
            autostart: vec![
                "wal -R",
                "picom --experimental-backends",
                "tbswitch normal",
                "nextcloud",
                "dunst",
                "redshift",
            ],
        }
    }
}

// Available user actions
#[derive(Clone)]
pub enum Action {
    MoveWindow(MoveResizeType),
    ResizeWindow(MoveResizeType),
    CloseWindow,
    Spawn(&'static str),
    GotoTag(usize),
    SetTag(usize),
    FullscreenToggle,
    Maximize(MaximizeSide),
    SwitchMons,
    FocusNext,
    CenterWin,
    Quit,
    Iconify,
    Uniconify,
    CycleTags(Dir),
    ResizeWin(i32, i32),
}

// Resize mode for MoveWindow and ResizeWindow
#[derive(Clone)]
pub enum MoveResizeType {
    Mouse,
    Value(i32, i32),
}

// Mode for Maximize
#[derive(Clone)]
pub enum MaximizeSide {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone)]
pub struct Binding {
    pub keycode: u8,
    pub modifier: ModMask,
    pub action: Action,
}

pub struct Config {
    pub bindings: Vec<Binding>,
    pub border_width: u32,
    pub autostart: Vec<&'static str>,
}
