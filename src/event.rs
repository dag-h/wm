use crate::client::Maximized;
use crate::config::{Action, MaximizeSide, MoveResizeType};
use crate::state::*;
use log::{error, info};
use std::process;
use x11rb::connection::Connection;
use x11rb::protocol::xproto::*;
use x11rb::protocol::Event;

pub fn handle_event<C: Connection>(mut state: State<C>, event: Event) -> Result<State<C>> {
    state = match event {
        Event::CreateNotify(_event) => state,
        Event::ConfigureRequest(event) => handle_configure_request(state, event)?,
        Event::MapRequest(event) => handle_map_request(state, event)?,
        Event::KeyPress(event) => handle_key_press(state, event)?,
        Event::KeyRelease(_) => state,
        Event::ButtonPress(event) => handle_button_press(state, event)?,
        Event::ButtonRelease(event) => handle_button_release(state, event)?,
        Event::MotionNotify(event) => handle_motion(state, event)?,
        Event::EnterNotify(event) => handle_enter_notify(state, event)?,
        Event::DestroyNotify(event) => handle_destroy_notify(state, event),
        Event::ClientMessage(event) => handle_client_message(state, event)?,
        Event::ConfigureNotify(_) => state,
        Event::MapNotify(event) => handle_map_notify(state, event)?,
        Event::UnmapNotify(event) => handle_unmap_notify(state, event)?,
        _ => state,
    };
    Ok(state)
}

fn handle_map_notify<C: Connection>(
    mut state: State<C>,
    event: MapNotifyEvent,
) -> Result<State<C>> {
    if !state.is_managing(event.window) {
        return Ok(state);
    }
    if let Some(client) = get_client_mut(&mut state.clients, event.window) {
        client.iconified = false;
    } else {
        error!("handle_map_notify: could not find client");
        return Ok(state);
    };
    state.iconified.remove(&event.window);
    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        event.window,
        state.atoms.WM_STATE,
        state.atoms.WM_STATE,
        &[1],
    )?;
    Ok(state)
}

fn handle_unmap_notify<C: Connection>(
    mut state: State<C>,
    event: UnmapNotifyEvent,
) -> Result<State<C>> {
    if !state.is_managing(event.window) {
        return Ok(state);
    }
    if let Some(client) = get_client_mut(&mut state.clients, event.window) {
        if client.iconified {
            state.iconified.insert(event.window);
        }
    } else {
        error!("handle_unmap_notify: could not find client");
        return Ok(state);
    }
    Ok(state)
}

fn handle_client_message<C: Connection>(
    mut state: State<C>,
    event: ClientMessageEvent,
) -> Result<State<C>> {
    info!("{:?}", event);
    let atom = state.atoms._NET_WM_STATE;
    if event.type_ == atom && event.format == 32 {
        let data32 = event.data.as_data32();
        let action = data32[0];
        let prop1 = data32[1];
        let atom = state.atoms._NET_WM_STATE_FULLSCREEN;
        if prop1 == atom {
            if let Some(client) = get_client(&state.clients, event.window) {
                let maximized = client.maximized;
                match action {
                    1 => {
                        // println!("tried to fullscreen");
                        match maximized {
                            Maximized::Full | Maximized::FromEvent => return Ok(state),
                            _ => {
                                state =
                                    set_win_state(state, Maximized::FromEvent, event.window, None)?
                            }
                        }
                    }
                    0 => {
                        // println!("tried to remove fullscreen");
                        match maximized {
                            Maximized::Full => return Ok(state),
                            Maximized::FromEvent => {
                                state = set_win_state(state, Maximized::None, event.window, None)?
                            }
                            _ => return Ok(state),
                        }
                    }
                    _ => return Ok(state),
                }
            }
        }
    }
    Ok(state)
}

fn handle_destroy_notify<C: Connection>(
    mut state: State<C>,
    event: DestroyNotifyEvent,
) -> State<C> {
    state.iconified.remove(&event.window);
    // unmanage will check if we are managing the window that was destroyed
    unmanage(state, event.event)
}

fn handle_configure_request<C: Connection>(
    state: State<C>,
    event: ConfigureRequestEvent,
) -> Result<State<C>> {
    // if we're not managing the window, simply grant the request with the desired settings.
    // otherwise, don't grant the request
    if !state.is_managing(event.window) {
        state.conn.configure_window(
            event.window,
            &ConfigureWindowAux::from_configure_request(&event),
        )?;
    }
    Ok(state)
}

fn handle_map_request<C: Connection>(
    mut state: State<C>,
    event: MapRequestEvent,
) -> Result<State<C>> {
    let geometry = state.conn.get_geometry(event.window)?.reply()?;
    state = manage_window(state, event.window, geometry)?;
    Ok(state)
}

fn handle_enter_notify<C: Connection>(
    mut state: State<C>,
    event: EnterNotifyEvent,
) -> Result<State<C>> {
    if event.event == state.get_root() {
        // for (i, mon) in state.mons.iter().enumerate() {
        //     if is_on_mon(event.root_x, event.root_y, mon) {
        //         state.focused_mon = i;
        //         x11rb::wrapper::ConnectionExt::change_property32(
        //             &state.conn,
        //             PropMode::REPLACE,
        //             state.get_root(),
        //             state.atoms._NET_CURRENT_DESKTOP,
        //             AtomEnum::CARDINAL,
        //             &[mon.tag as u32],
        //         )?
        //         .check()?;
        //     }
        // }
        Ok(state)
    } else {
        state = focus(state, event.event)?;
        Ok(state)
    }
}

fn handle_key_press<C: Connection>(mut state: State<C>, event: KeyPressEvent) -> Result<State<C>> {
    // TODO find way to avoid clone
    let bindings = state.config.bindings.clone();
    for binding in bindings {
        if binding.keycode == event.detail && binding.modifier == ModMask::from(event.state) {
            match &binding.action {
                Action::Spawn(cmd) => spawn(cmd)?,
                Action::CloseWindow => state = kill_win(state, None)?,
                Action::GotoTag(tag) => state = goto_tag(state, tag - 1)?,
                Action::SetTag(tag) => state = set_tag(state, tag - 1, None)?,
                // Action::FullscreenToggle => state = fullscreen_toggle(state, None)?,
                Action::FullscreenToggle => state = maximize_toggle(state, Maximized::Full, None)?,
                Action::Maximize(MaximizeSide::Left) => {
                    state = maximize_toggle(state, Maximized::Left, None)?
                }
                Action::Maximize(MaximizeSide::Right) => {
                    state = maximize_toggle(state, Maximized::Right, None)?
                }
                // Action::Maximize(side) => state = maximize_side(state, side)?,
                Action::SwitchMons => state = switchmons(state)?,
                Action::FocusNext => state = focus_next(state)?,
                Action::CenterWin => state = center_win(state)?,
                Action::Quit => process::exit(1),
                Action::Iconify => state = iconify(state)?,
                Action::Uniconify => state = uniconify(state)?,
                Action::CycleTags(dir) => state = cycle_tags(state, *dir)?,
                Action::ResizeWin(x, y) => state = resize_win(state, *x, *y)?,
                Action::MoveWindow(MoveResizeType::Value(x, y)) => {
                    state = move_win(state, None, *x, *y, MoveType::Inc)?
                }
                _ => (),
            };
            break;
        }
    }
    Ok(state)
}

fn handle_button_press<C: Connection>(
    mut state: State<C>,
    event: ButtonPressEvent,
) -> Result<State<C>> {
    let cfg = ConfigureWindowAux::new().stack_mode(StackMode::ABOVE);
    state.conn.configure_window(event.event, &cfg)?;

    let mut quadrant = 0;
    if let Some(client) = get_client(&state.clients, event.event) {
        if event.event_x < client.width as i16 / 2 {
            if event.event_y < client.height as i16 / 2 {
                quadrant = 2;
            } else {
                quadrant = 3;
            }
        } else if event.event_y < client.height as i16 / 2 {
            quadrant = 1;
        } else {
            quadrant = 4;
        }
    }
    if quadrant == 0 {
        error!("handle_button_press: couldn't determine correct quadrant");
        return Ok(state);
    }

    let x_offs = event.root_x;
    let y_offs = event.root_y;
    state.dragging = Some(MouseMotion {
        x_offs,
        y_offs,
        quadrant,
    });

    state.conn.grab_pointer(
        false,
        event.event,
        MOUSE_MASK,
        GrabMode::ASYNC,
        GrabMode::ASYNC,
        0_u32,
        0_u32,
        0_u32,
    )?;
    state.motion_button = Some(event.detail);
    Ok(state)
}

fn handle_button_release<C: Connection>(
    mut state: State<C>,
    _event: ButtonReleaseEvent,
) -> Result<State<C>> {
    state.dragging = None;
    state.motion_button = None;
    state.conn.ungrab_pointer(0_u32)?;
    Ok(state)
}

fn handle_motion<C: Connection>(mut state: State<C>, event: MotionNotifyEvent) -> Result<State<C>> {
    for (i, mon) in state.mons.iter().enumerate() {
        if is_on_mon(event.root_x, event.root_y, mon) {
            state.focused_mon = i;
            state.update_curr_tag()?;
            if event.event == state.get_root() {
                return Ok(state);
            }
        }
    }

    // don't try to handle motion events faster than a regular monitor could refresh
    if event.time - state.last_motion < 1000 / 60 {
        return Ok(state);
    }
    state.last_motion = event.time;

    // we don't allow moving or resizing of maximized windows
    if let Some(client) = get_client(&state.clients, event.event) {
        match client.maximized {
            Maximized::None => (),
            _ => return Ok(state),
        }
    }

    state = match state.motion_button {
        // TODO make modular
        Some(1) => move_with_mouse(state, event)?,
        Some(3) => resize_with_mouse(state, event)?,
        _ => state,
    };

    let mouse = if let Some(mouse) = &state.dragging {
        mouse
    } else {
        return Ok(state);
    };

    state.dragging = Some(MouseMotion {
        x_offs: event.root_x,
        y_offs: event.root_y,
        quadrant: mouse.quadrant,
    });
    Ok(state)
}
