#[derive(Debug, thiserror::Error)]
pub enum WMError {
    #[error(transparent)]
    X11ReplyError(#[from] x11rb::errors::ReplyError),
    #[error(transparent)]
    X11ReplyOrIdError(#[from] x11rb::errors::ReplyOrIdError),
    #[error(transparent)]
    X11ConnectionError(#[from] x11rb::errors::ConnectionError),
    #[error(transparent)]
    CmdError(#[from] std::io::Error),
    #[error(transparent)]
    ParseUtf8Error(#[from] std::str::Utf8Error),
    #[error(transparent)]
    ParseToIntError(#[from] std::num::ParseIntError),
    #[error("{func:?}: could not find {obj:?}")]
    FindError { func: String, obj: String },
}
