use log::{error, info};
use simplelog::WriteLogger;
use std::fs::File;
use std::process;
use wm::config::{Binding, Config};
use wm::errors::WMError;
use wm::event::handle_event;
use wm::monitor::Monitor;
use wm::state::{autostart, map_unmapped_windows, Atoms, State};
use x11rb::connection::Connection;
use x11rb::cursor::Handle;
use x11rb::errors::ReplyError;
use x11rb::protocol::xproto::{
    AtomEnum, ChangeWindowAttributesAux, ConnectionExt, CreateWindowAux, Cursor, EventMask,
    GrabMode, PropMode, Screen, Window, WindowClass,
};
use x11rb::protocol::ErrorKind;
use x11rb::resource_manager::Database;

fn main() -> Result<(), WMError> {
    let _ = simplelog::CombinedLogger::init(vec![
        WriteLogger::new(
            simplelog::LevelFilter::Info,
            simplelog::Config::default(),
            File::create("wm.log").unwrap(),
        ),
        WriteLogger::new(
            simplelog::LevelFilter::Error,
            simplelog::Config::default(),
            File::create("wm.log").unwrap(),
        ),
    ]);

    // TODO handle and log error
    let (conn, screen_num) = x11rb::connect(None).unwrap();

    let screen = &conn.setup().roots[screen_num];

    let root = screen.root;

    let cursor = setup_cursor(&conn, screen_num)?;

    match init_wm(&conn, screen, cursor) {
        Ok(_) => println!("WM successfully started"),
        Err(err) => {
            error!("An error occurred when starting WM: {}", err);
            process::exit(1);
        }
    }

    let config: Config = Config::default();

    let mons: Vec<Monitor>;
    if has_xinerama(&conn)? {
        info!("Xinerama detected");
        mons = setup_monitors(&conn)?;
        info!("Monitors: {}", mons.len());
    } else {
        mons = vec![Monitor::from(screen)];
    }

    // TODO handle
    let atoms = Atoms::new(&conn)?.reply()?;

    let mut state = State::new(conn, screen_num, mons, config, atoms);

    set_supported_hints(&state)?;

    autostart(&state)?;

    grab_keys(&state.conn, root, &state.config.bindings)?;
    state = map_unmapped_windows(state)?;

    // main event loop
    // let running = true;
    loop {
        state.conn.flush()?;
        let event = state.conn.wait_for_event()?;
        let mut event_opt = Some(event);
        while let Some(event) = event_opt {
            // TODO: actually handle event errors before they bubble up to main
            state = handle_event(state, event)?;

            event_opt = state.conn.poll_for_event()?;
        }
    }
}

fn set_supported_hints<C: Connection>(state: &State<C>) -> Result<(), WMError> {
    let atoms = vec![
        state.atoms._NET_WM_STATE,
        state.atoms._NET_WM_STATE_FULLSCREEN,
        state.atoms._NET_CLIENT_LIST,
        state.atoms._NET_NUMBER_OF_DESKTOPS,
        state.atoms._NET_CURRENT_DESKTOP,
        state.atoms._NET_DESKTOP_VIEWPORT,
        state.atoms._NET_DESKTOP_NAMES,
        state.atoms.WM_PROTOCOLS,
        state.atoms.WM_DELETE_WINDOW,
    ];

    for atom in atoms {
        let data: &[u32] = &[atom];
        x11rb::wrapper::ConnectionExt::change_property32(
            &state.conn,
            PropMode::APPEND,
            state.get_root(),
            state.atoms._NET_SUPPORTED,
            AtomEnum::ATOM,
            data,
        )?;
    }

    let child = state.conn.generate_id()?;
    state.conn.create_window(
        x11rb::COPY_DEPTH_FROM_PARENT,
        child,
        state.get_root(),
        0,
        0,
        1,
        1,
        0,
        WindowClass::INPUT_OUTPUT,
        0,
        &CreateWindowAux::new().override_redirect(1),
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_SUPPORTING_WM_CHECK,
        AtomEnum::WINDOW,
        &[child],
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        child,
        state.atoms._NET_SUPPORTING_WM_CHECK,
        AtomEnum::WINDOW,
        &[child],
    )?;

    x11rb::wrapper::ConnectionExt::change_property8(
        &state.conn,
        PropMode::REPLACE,
        child,
        state.atoms._NET_WM_NAME,
        AtomEnum::STRING,
        b"wm",
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_NUMBER_OF_DESKTOPS,
        AtomEnum::CARDINAL,
        &[9],
    )?;

    let names = b"1\x002\x003\x004\x005\x006\x007\x008\x009\x00";
    x11rb::wrapper::ConnectionExt::change_property8(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_DESKTOP_NAMES,
        AtomEnum::STRING,
        names,
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_CURRENT_DESKTOP,
        AtomEnum::CARDINAL,
        &[0_u32],
    )?;

    x11rb::wrapper::ConnectionExt::change_property32(
        &state.conn,
        PropMode::REPLACE,
        state.get_root(),
        state.atoms._NET_DESKTOP_VIEWPORT,
        AtomEnum::CARDINAL,
        &[
            0_u32, 0_u32, 1920_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32,
            0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32, 0_u32,
        ],
    )?;

    Ok(())
}

fn has_xinerama<C: Connection>(conn: &C) -> Result<bool, WMError> {
    match x11rb::protocol::xinerama::is_active(conn)?.reply()?.state {
        1 => Ok(true),
        _ => Ok(false),
    }
}

fn setup_monitors<C: Connection>(conn: &C) -> Result<Vec<Monitor>, WMError> {
    let mut screens = x11rb::protocol::xinerama::query_screens(conn)?
        .reply()?
        .screen_info;
    let mut mons = Vec::new();
    for (i, scr) in screens.iter_mut().enumerate() {
        let mut mon = Monitor::from(*scr);
        info!("Setting monitor tag to {}", i);
        mon.idx = i;
        mon.tag = i;
        mons.push(mon);
    }
    Ok(mons)
}

fn setup_cursor<C: Connection>(conn: &C, screen_num: usize) -> Result<Cursor, WMError> {
    let db = Database::new_from_default(conn)?;
    let handle = Handle::new(conn, screen_num, &db)?.reply()?;
    // for list of cursors: https://code.woboq.org/gtk/include/X11/cursorfont.h.html
    match handle.load_cursor(conn, "left_ptr") {
        Ok(cursor) => Ok(cursor),
        Err(err) => Err(err.into()),
    }
}

fn init_wm<C: Connection>(conn: &C, screen: &Screen, cursor: Cursor) -> Result<(), WMError> {
    let change = ChangeWindowAttributesAux::default()
        .event_mask(
            EventMask::SUBSTRUCTURE_REDIRECT
                | EventMask::SUBSTRUCTURE_NOTIFY
                | EventMask::STRUCTURE_NOTIFY
                | EventMask::ENTER_WINDOW
                | EventMask::POINTER_MOTION
                | EventMask::BUTTON_PRESS
                | EventMask::BUTTON_RELEASE
                | EventMask::KEY_PRESS
                | EventMask::KEY_RELEASE,
        )
        .cursor(cursor);

    let res = conn.change_window_attributes(screen.root, &change)?.check();
    match res {
        Err(ReplyError::X11Error(error)) => {
            if error.error_kind == ErrorKind::Access {
                error!("Another WM is already running.");
                process::exit(1);
            } else {
                Ok(())
            }
        }
        Err(other) => Err(other.into()),
        Ok(()) => Ok(()),
    }
}

fn grab_keys<C: Connection>(conn: &C, win: Window, bindings: &[Binding]) -> Result<(), WMError> {
    for b in bindings {
        conn.grab_key(
            false,
            win,
            b.modifier,
            b.keycode,
            GrabMode::ASYNC,
            GrabMode::ASYNC,
        )?;
    }
    Ok(())
}

// fn _str_to_modmask(s: &str) -> Option<ModMask> {
//     let mut mods = s.split("+");
//     let mut final_mask = _match_modmask(mods.next()?)?;

//     for m in mods {
//         final_mask = final_mask | _match_modmask(m)?;
//     }
//     Some(final_mask)
// }

// fn _match_modmask(s: &str) -> Option<ModMask> {
//     match s {
//         "mod1" => Some(ModMask::M1),
//         "mod2" => Some(ModMask::M2),
//         "mod3" => Some(ModMask::M3),
//         "mod4" => Some(ModMask::M4),
//         "mod5" => Some(ModMask::M5),
//         "shift" => Some(ModMask::SHIFT),
//         "ctrl" => Some(ModMask::CONTROL),
//         _ => None,
//     }
// }
