use x11rb::protocol::xinerama::ScreenInfo;
use x11rb::protocol::xproto::Screen;

pub type MonsIdx = usize;
pub type TagNum = usize;

pub struct Monitor {
    pub tag: TagNum,
    pub x_org: i16,
    pub y_org: i16,
    pub width: u16,
    pub height: u16,
    pub idx: usize,
}

impl From<ScreenInfo> for Monitor {
    fn from(scr_info: ScreenInfo) -> Self {
        Monitor {
            tag: 0,
            x_org: scr_info.x_org,
            y_org: scr_info.y_org,
            width: scr_info.width,
            height: scr_info.height,
            idx: 0,
        }
    }
}

impl From<&Screen> for Monitor {
    fn from(scr: &Screen) -> Self {
        Monitor {
            tag: 0,
            x_org: 0,
            y_org: 0,
            width: scr.width_in_pixels,
            height: scr.height_in_pixels,
            idx: 0,
        }
    }
}
