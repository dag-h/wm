use crate::monitor::{Monitor, TagNum};
use crate::utils;
use log::info;
use x11rb::protocol::xproto::GetGeometryReply;
use x11rb::protocol::xproto::Window;

// struct representing a window currently mapped by the wm
pub struct Client {
    pub window: Window,
    // positions are relative to current monitor
    pub x: i16,
    pub y: i16,
    pub width: u16,
    pub height: u16,
    pub tag: TagNum,
    pub fullscreen: bool,
    pub maximized: Maximized,
    pub iconified: bool,
    pub old_geom: Geom,
}

pub struct Geom {
    pub x: i16,
    pub y: i16,
    pub width: u16,
    pub height: u16,
}

impl Geom {
    pub fn new(x: i16, y: i16, width: u16, height: u16) -> Self {
        Geom {
            x,
            y,
            width,
            height,
        }
    }
}

impl Client {
    // creates a new WindowState by getting geometry info from the given window
    pub fn new(window: Window, geom: GetGeometryReply, mon: &Monitor) -> Self {
        info!("Getting relative pos in constructor...");
        let (x, y) = utils::get_relative_pos(mon, geom.x as i32, geom.y as i32);
        Client {
            window,
            x: x as i16,
            y: y as i16,
            width: geom.width,
            height: geom.height,
            tag: 0,
            fullscreen: false,
            maximized: Maximized::None,
            iconified: false,
            old_geom: Geom::new(x as i16, y as i16, geom.width, geom.height),
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum Maximized {
    Full,
    Left,
    Right,
    None,
    FromEvent,
}
